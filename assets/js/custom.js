var mouseX = 0, mouseY = 0, limitX = 100-70, limitY = 650-40;
    $(window).mousemove(function(e){
        var offset = $('.flotNavigation').offset();
        mouseX = Math.min(e.pageX - offset.left, limitX);
        mouseY = Math.min(e.pageY - offset.top, limitY);
        if (mouseX < 0) mouseX = 0;
        if (mouseY < 0) mouseY = 0;
    });

    var btnMenu = $("#btnMenu");
    var xp = 0, yp = 0;
    var loop = setInterval(function(){
        // change 12 to alter damping higher is slower
        xp += (mouseX - xp) / 30;
        yp += (mouseY - yp) / 30;
        btnMenu.css({left:xp, top:yp});
        
}, 30);

// $(document).mousemove(function(e) {
//     $('#btnMenu').offset({        
//         top: e.pageY + 20
//     });
// });
// function tick(event) {
//     var xDistance = stage.getStage().mouseX - ship.x;
//     var yDistance = stage.getStage().mouseY - ship.y;
//     var distance = Math.sqrt(xDistance * xDistance + yDistance * yDistance);
//      if (distance > 1) {
//          ship.x += xDistance * easingAmount;
//          ship.y += yDistance * easingAmount;
//      }
//      stage.update();
//  }
/*--------------------------------------------------------------
  3. Full Page
--------------------------------------------------------------*/
$('#fullpage').fullpage({
    hybrid:true,
    sectionSelector: '.secMain',
    fitToSection: false,
    responsiveWidth: 768,
    scrollingSpeed: 500,
    easing: 'easeInOutCubic',
    easingcss3: "ease",
    normalScrollElementTouchThreshold: 5,
    lazyLoading: true,
    afterResponsive: function(isResponsive){
    }
});

/*--------------------------------------------------------------
  4. Fixed Header
--------------------------------------------------------------*/
$(function() {
    var header = $('header');
    $(window).scroll(function() {
        var scroll = $(window).scrollTop();

        if (scroll >= 500) {
            header.addClass('fixedHeader');
        } else {
            header.removeClass('fixedHeader');
        }
    });
})

/*--------------------------------------------------------------
  5. Carousel
--------------------------------------------------------------*/
$(document).ready(function() {
    $('#container2').carousel({
        num: 5,
        maxHeight: 432,
        maxWidth: 539,
        distance: 200,
        showTime: 5000
    });
});

/*--------------------------------------------------------------
  6. 3rd Partner Carousel
--------------------------------------------------------------*/
$('#partnercarousel').owlCarousel({
    loop:true,
    margin:10,
    nav:true,
    navText:["<i class='fa fa-arrow-left'></i>","<i class='fa fa-arrow-right'></i>"],
    dots:false,
    autoPlay:true,
    responsive:{
        0:{
            items:4
        }
    }
})

/*--------------------------------------------------------------
  7. Add Class
--------------------------------------------------------------*/
$('#btnMenu').hover(function () {
        $('body').find('.blackOverlay').addClass('overlay');
        $('body').find('#menu').addClass('transtion');
    }, 
);   

/*--------------------------------------------------------------
  8. Remove Class
--------------------------------------------------------------*/
$('#closeMenuBtn').hover(function () {
    $('body').find('.blackOverlay').removeClass('overlay');
    $('body').find('#menu').removeClass('transtion');
    }, 
);
/*--------------------------------------------------------------
  9. Tabs
--------------------------------------------------------------*/
    $(document).ready(function() {
        $('ul.tabs').tabs('select_tab', 'aboutus');
    });

/*--------------------------------------------------------------
  10. mobile toggle
--------------------------------------------------------------*/
$(function () {
    var pull = $('.mobile-toggle');
    menu = $('ul.navigation');
    menuHeight = menu.height();

    $(pull).on('click', function (e) {
        e.preventDefault();
        pull.toggleClass("open");
        menu.toggleClass("nav-open");
    });

    $(window).resize(function () {
        var w = $(window).width();
        if (w > 767 && menu.is(':hidden')) {
            menu.removeAttr('style');
        }
    });
});
/*--------------------------------------------------------------
  11. Loader
--------------------------------------------------------------*/

$(window).load(function() {
    $(".loader").fadeOut("slow");
});
/*--------------------------------------------------------------
  12. on scroll class add
--------------------------------------------------------------*/
$(window).scroll(function() {    
    var scroll = $(window).scrollTop();    
    if (scroll <= 500) {
        $("header").addClass("fixedHeader");
    }
    });